/**
 * main js
 */


function InitApplication()
{



}




// Ajax Functions der Aktionen der EditPage


/**
 * Der im OptionsMenu ausgewählte Film wir in die Input-Elemente geladen
 * @constructor
 */

function LoadMovie()
{
    // Die Movie Id aus dem optionsMenu der EditPage
    var   id = $("#optionMovies").val();


        $.ajax({ url: 'processAjax.php',
            type: "POST",
            data: { funcName: 'LoadMovie',movieId: id },
            dataType : 'json',

            success: function(output) {

               // console.log(output['id']);
                $("#id").val(id);
                $("#titel").val(output['titel']);
                $("#jahr").val(output['jahr']);
                $("#dauer").val(output['dauer']);
                $("#beschreibung").val(output['beschreibung']);
                $("#rating").val(output['rating']);
                $("#imdbid").val(output['imdbid']);
                $("#bild").val(output['bild']);
                $("#regisseur").val(output['regisseur']);
                $("#genres").val(output['genre']);
                $("#actors").val(output['schauspieler']);

            }

        });


}


/**
 * Ein neuer Film wird in die Datenbank geladen Inhalt der input-Elemente
 *
 * @constructor
 */
function NewMovie()
{

    // FilmDaten Object das an php übergeben wird

    var filmData = {titel:$("#titel").val(), jahr:$("#jahr").val(), dauer:$("#dauer").val(), beschreibung:$("#beschreibung").val(),
                    rating:$("#rating").val(), imdbid:$("#imdbid").val(),bild:$("#bild").val(),
                    regisseur:$("#regisseur").val(),genre:$("#genres").val(), schauspieler:$("#actors").val()  };



    // Ajax Aufruf zum Speichern eines neuen Films
    $.ajax({
        url: 'processAjax.php',
        data: { funcName: 'NewMovie',filmData: filmData},
        dataType : 'json',
        success: function(output) {


            // Das OptionsMenu wird aktualisiert ( neuer Film-Eintrag )
            $("#optionMovies").empty();

            for (var i = 0; i< output.length; i++){

                $("#optionMovies").append("<option value='" + output[i].id + "'>" + output[i].titel + "</option>");
            }


            alert('Insert successful');
        }
    });

}

/**
 * Ein update eines ausgewählten Film wird in die Datenbank geschrieben
 * @constructor
 */
function UpdateMovie()
{
   // FilmDaten Object das in php übergeben wird

    var filmData = { id:$("#id").val(),titel:$("#titel").val(), jahr:$("#jahr").val(), dauer:$("#dauer").val(), beschreibung:$("#beschreibung").val(),
        rating:$("#rating").val(), imdbid:$("#imdbid").val(),bild:$("#bild").val(), regisseur:$("#regisseur").val(),genre:$("#genres").val(), schauspieler:$("#actors").val()  };

   //console.log(filmData);

    // Ajax Aufruf zum Update eines Films
    $.ajax({
        url: 'processAjax.php',
        data: { funcName: 'UpdateMovie',filmData: filmData},
        success: function(output) {

           alert(output);
        }
    });

}

/**
 * Ein ausgewählter Film wird aus der Datenbank gelöscht
 *
 * @constructor
 */
function DeleteMovie()
{

//     FilmDaten Object das in php übergeben wird

    var movieId = $("#id").val();

    var filmData = { id:$("#id").val(),titel:$("#titel").val(), jahr:$("#jahr").val(), dauer:$("#dauer").val(), beschreibung:$("#beschreibung").val(),
        rating:$("#rating").val(), imdbid:$("#imdbid").val(),bild:$("#bild").val(), regisseur:$("#regisseur").val(),genre:$("#genres").val(), schauspieler:$("#actors").val()  };

    // Ajax Aufruf zum Delete eines Films
    $.ajax({
        url: 'processAjax.php',
        data: { funcName: 'DeleteMovie',filmData: filmData},
        success: function(output) {

            $("#id").val('');
            $("#titel").val('');
            $("#jahr").val('');
            $("#dauer").val('');
            $("#beschreibung").val('');
            $("#rating").val('');
            $("#imdbid").val('');
            $("#bild").val('');
            $("#regisseur").val('');
            $("#genres").val('');
            $("#actors").val('');

            // Der gelöschte Film wird aus dem OptionMenu entfernt
            $("#optionMovies option[value='" + movieId + "']").remove();


            alert(output);
        }
    });

}
