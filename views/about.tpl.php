<?php
/**
 * About Template
 */

?>

<!DOCTYPE html>
<html>

<?php require_once '_head.tpl.php';?>


<body>

<?php require_once '_nav.tpl.php';?>





            <div class="container">


                <h1>Autor : Thomas Barwanietz</h1>

                Zweck und Beschreibung :
                Filmdatenbank ist eine Online Datenbank mit CRUD-Funktionalität
                Die Webanwendung ist nach dem MVC - Design Pattern Object Orientiert entwickelt.
                Die Abbildung der Datenbank zum objektorientierten Design erfolgt über das DataMapper
                Prinzip. Wobei es zur jeder Tabelle eine DatenKlasse und für diese Klasse genau eine DataMapper
                Klasse gibt. Der Datenbank zugriff erfolgt auf dem Server mit PHP über die PDO - Klasse

            </div>

<?php require_once '_footer.tpl.php'; ?>

<?php require_once '_scripts.tpl.php'; ?>

</body>

</html>
