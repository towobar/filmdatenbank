<?php
/**
 * Home Template
 */

?>

<!DOCTYPE html>
<html>

<?php require_once '_head.tpl.php';?>




<body>

<?php require_once '_nav.tpl.php';?>



    <div class="jumbotron text-center">

        <h1 class="head" >Filmdatenbank</h1>
        <p style="color:green !important">Speichern,Bearbeiten und Anlegen von Filmdaten</p>

    </div>


<?php require_once '_footer.tpl.php'; ?>


<?php require_once '_scripts.tpl.php'; ?>

</body>
</html>
