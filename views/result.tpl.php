<!DOCTYPE html>
<html>

<?php require_once '_head.tpl.php'; ?>

<body>


<?php require_once '_nav.tpl.php'; ?>

<div class="container">

    <div class="row">
        <div class="col-md-12">
            <h1>Movie Overview</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <?php if (isset($_GET['id'])) {
                ?>


                <img src="assets/images/covers/<?php echo $movieOverview->getBild() . '.jpg'; ?>" alt="">


                <div style="margin-bottom:20px;">

                    <span id="title"><?php echo $movieOverview->getTitel(); ?></span><span> ( <?php echo $movieOverview->getJahr(); ?> )</span><br><br>
                    <span><?php echo $movieOverview->getDauer(); ?> Min</span><span> |  <?php echo $movieOverview->getGenre(); ?></span><br><br>
                    <span>Rating :  </span><span><?php echo $movieOverview->getRating(); ?></span><br><br>
                    <div>Plot :</div>
                    <br>
                    <div><?php echo $movieOverview->getBeschreibung(); ?></div>
                    <br> <span>DIRECTOR : </span><span><?php echo $movieOverview->getRegisseur(); ?></span>
                    <br><br> <span>ACTORS : </span><span><?php echo $movieOverview->getSchauspieler(); ?></span>

                </div>

            <?php } else {
                //redirect('index.php');
                header("Location: index.php");

            }
            ?>

        </div>
    </div>

    <?php require_once '_footer.tpl.php'; ?>


    <?php require_once '_scripts.tpl.php'; ?>

</body>
</html>
