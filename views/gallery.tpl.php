<?php
/**
 * Gallery Template
 */

?>

<!DOCTYPE html>
<html lang="">

<?php require_once '_head.tpl.php'; ?>


<body>

<?php require_once '_nav.tpl.php'; ?>

<div class="container">

    <div class="row" style="margin-bottom:20px!important;">

        <div class="col-lg-12">
            <h1>Film Gallery</h1>
        </div>

        <div class="col-md-12">

            <?php foreach ($movieList as $movie) { ?>

                <a href="index.php?action=result&id=<?php echo $movie->getId() ;?>">

                    <img src="assets/images/covers/<?php echo $movie->getBild() . '.jpg' ;?>"  width="136px" height="202px" alt="">

                </a>

            <?php } ?>
        </div>

    </div>
</div>



<?php require_once '_footer.tpl.php'; ?>

<?php require_once '_scripts.tpl.php'; ?>

</body>
</html>
