<?php
/**
 * Edit Template
 */

?>

<!DOCTYPE html>
<html>

<?php require_once '_head.tpl.php';?>


<body>

<?php require_once '_nav.tpl.php';?>



<div class="container">

    <div class="row">
        <div class="col-sm-12">
            <h3>Bearbeiten von Filmdaten</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <h3>FilmDaten</h3>

            <input type="hidden" name="id" id="id" value=""/>


            <table>
                <tr>
                    <td><label for="titel">Titel</label></td><td><input type="text" name="titel" id="titel" value="" /></td>
                </tr>
                <tr>
                    <td><label for="jahr2">Jahr</label></td><td><input type="text" name="jahr" id="jahr" value=""  /></td>
                </tr>

                <tr>
                    <td><label for="dauer">Dauer</label></td><td><input type="text" name="dauer" id="dauer" value=""   /></td>
                </tr>
                <tr>
                    <td><label for="beschreibung">Beschreibung</label></td><td><input type="text" name="beschreibung" id="beschreibung" value=""   /></td>
                </tr>


                <tr>
                    <td><label for="rating">Rating</label></td><td><input type="text" name="rating" id="rating" value=""  /></td>
                </tr>
                <tr>
                    <td><label for="imdbid">ImDbID</label></td><td><input type="text" name="imdbid" id="imdbid" value=""  /></td>
                </tr>
                <tr>
                    <td><label for="cover">Cover</label></td><td><input type="text" name="bild" id="bild"  value=""  /></td>
                </tr>
                <tr>
                    <td><label for="regisseur">Regisseur</label></td><td><input type="text" name="regisseur" id="regisseur" value=""  /></td>
                </tr>
                <tr>
                    <td><label for="genre">Genre</label></td><td><input type="text" name="genre" id="genres" value=""   /></td>
                </tr>
                <tr>
                    <td><label for="actors">Actors</label></td><td><input type="text" name="schauspieler" id="actors" value=""   /></td>
                </tr>


                //TODO: Delete Entry
                <tr>
                    <td><label for="actors">Actors</label></td><td><input type="date" name="schauspieler" id="actors" value=""   /></td>
                </tr>
                
                
                
            </table>
        </div>
        <div class="col-sm-6">
            <h3>Aktionen</h3>

            <div class="row rowMargin">

                <div class="col-sm-12">

                    <button type="button" onclick="LoadMovie()" class="btn btn-default">Load</button>

                    <select name="optionMovies" id="optionMovies" >
                        <?php foreach ($movieList as $movie) { ?>
                            <option

                                value="<?php echo $movie->getId(); ?>"

                                <?php if ($movie->getId() == $movieID){ ?>

                                    selected="selected"

                                <?php } ?>
                                >
                                <?php echo $movie->getTitel(); ?></option>

                        <?php } ?>
                    </select>

              </div>
            </div>

            <div class="row rowMargin">

                <div class="col-sm-12">
                    <button type="button" onclick="NewMovie()" class="btn btn-default">New</button>
                    <button type="button" onclick="DeleteMovie()" class="btn btn-default">Delete</button>
                    <button type="button" onclick="UpdateMovie()" class="btn btn-default">Update</button>

               </div>
            </div>

        </div>
    </div>
</div>



<?php require_once '_footer.tpl.php'; ?>

<?php require_once '_scripts.tpl.php'; ?>

</body>

</html>