<?php
/**
 *  MVC - Controller
 *  der Filmdatenbank
 */

// autoload + db-conf
require_once __DIR__ . '/inc/init.inc.php';


use App\Models\DB1;
use App\Models\Movie;
use App\Models\MovieMapper;


// Singleton Pattern Database Class
$dBCon = DB1::getInstance();


$dBMessage = 'Ready for commands ...';


$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : null;

$view = $action;

    switch($action) {

    case 'gallery':

        $movieMapper = new MovieMapper($dBCon);

        $movieList =  $movieMapper->getMoviesCoverAndId();

        break;

    case 'result':

         // Es wurde keine id übergeben : init id=1
        $id = (isset($_REQUEST['id'])) ? $_REQUEST['id'] : 1;

        $movieMapper = new MovieMapper($dBCon);

        $movieOverview = $movieMapper->getMovieOverview($id);

        break;


        case 'edit':

            // Noch zu bearbeiten

            $movieMapper = new MovieMapper($dBCon);

            $movieList = $movieMapper->getMoviesTitleAndId();

            break;


        case 'imprint' :

            break;

        case 'contact' :

            break;

        case 'about' :

            break;

        default:

     $movieMapper = new MovieMapper($dBCon);

     $movieList = $movieMapper->getMoviesTitleAndId();

     $view = 'home';

        break;
    }


require_once 'views/' . $view . '.tpl.php';


