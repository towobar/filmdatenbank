<?php
/**
 * Ajax Functions -> Php
 */

require_once __DIR__ . '/inc/init.inc.php';


use App\Models\DB1;
use App\Models\Movie;
use App\Models\MovieMapper;



switch ($_REQUEST['funcName']) {

    case 'LoadMovie' :

       AjaxLoadMovie();

        break;


    case 'NewMovie' :

        AjaxNewMovie();

        break;


    case 'UpdateMovie' :

        AjaxUpdateMovie();

        break;


    case 'DeleteMovie' :

        AjaxDeleteMovie();

        break;

    default :

        break;


}


function AjaxLoadMovie()
{

    $dBCon = DB1::getInstance();

    $movieId =  $_REQUEST['movieId'];

    $movieMapper = new MovieMapper($dBCon);

    $movie = $movieMapper->getMovieOverview($movieId);

    // Die Movie-class implements JsonSerializable
    echo  json_encode($movie);


}

function AjaxNewMovie()
{

    // Die FilmDaten   in einem  assoziativen Array ( wurde als javascript object übergeben )
    $filmData = $_REQUEST['filmData'];

    $dBCon = DB1::getInstance();

    $movie = new Movie($filmData);

    $movieMapper = new MovieMapper($dBCon);

    $dBMessage = $movieMapper->insertMovie($movie);

    // Liste der MovieTitel + Id zum asynchronen updaten des selectMenus mit den FilmTitel
    $newMovieList = $movieMapper->getMoviesTitleAndId();


    // Die Movie-class implements JsonSerializable

    echo  json_encode($newMovieList);




}


function AjaxUpdateMovie()
{

    // Die FilmDaten   in einem  assoziativen Array ( wurde als javascript object übergeben )
    $filmData = $_REQUEST['filmData'];

    $dBCon = DB1::getInstance();


    $movie = new Movie($filmData);

    $movieMapper = new MovieMapper($dBCon);


    $dBMessage = $movieMapper->updateMovie($movie);

    echo  $dBMessage;




}


function AjaxDeleteMovie()
{
    // Die FilmDaten   in einem  assoziativen Array ( wurde als javascript object übergeben
    $filmData = $_REQUEST['filmData'];

    $dBCon = DB1::getInstance();


    $movie = new Movie($filmData);

    $movieMapper = new MovieMapper($dBCon);


    $dBMessage = $movieMapper->deleteMovie($movie);


    echo $dBMessage;
}
