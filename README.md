# README #

## This is a demo application for the MVC-Design Pattern
* * *
### What is this repository for? ###

* Movie Database is an online database with CRUD functionality.
  The web application is developed Object Oriented according to the MVC - Design Pattern . 
* The mapping of the database to the object-oriented design is done via the DataMapper principle. 
  There is a data class for each table and exactly one DataMapper class for each table class. 
* The database is accessed on the server with PHP via the PDO class.
  The connection to the database via the PDO object is realized with the singleton DesignPattern.
* The autoloading of the specific classes is realized with composer ("psr-4") and namespaces.
* The CRUD functionalities are realized via ajax with javscript and the jquery framework
* The frontend was realized responsive with bootstrap

![alternativetext](assets/images/Filmdatenbank.png)