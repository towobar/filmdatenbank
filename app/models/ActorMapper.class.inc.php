<?php

namespace models;


class ActorMapper {


    private $dB = null;

    public function __construct($dB)
    {
        // Datenbank Object initialisieren

        $this->dB = $dB;

        //var_dump($this->dB);
    }


    public    function getActorsNameAndId()
    {



        $objectArray = array();

        $sql = "SELECT id,name  FROM schauspieler ORDER BY name ASC ";

        $dbSelect = $this->dB->prepare($sql);

        $dbSelect->execute();


        try{

            while($row = $dbSelect->fetch()){

                // Klasse initialisieren und Daten (assoziatives array) übergeben
                // Die setter werden im __construct von Movie über das array befüllt

                $actor = new Actor($row);

                // Object an das array anhängen
                $objectArray[] = $actor;

            }


        }
        catch(PDOException $e)
        {

            $objectArray = null;
        }

        unset($dbSelect);


        return $objectArray;


    }










}