<?php
/**
 * DB Class PDO
 */

namespace App\Models;
use PDO;

class DB1 {

    private static $_singleton;
    private $_connection;


    private  function __construct()
    {

        // Constants are defined in db_configuration-local.inc.php

        $dsn = 'mysql:host=' . DB_HOST . ';port=' . DB_PORT . ';dbname=' . DB_NAME;

        try
        {
            $this->_connection = new PDO($dsn, DB_USER, DB_PASS, array(

                PDO::ATTR_PERSISTENT => true,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true
            ));

            // Damit ü ä etc. richtig dargestellt werden !
            $this->_connection->exec('SET NAMES utf8');



        }
        catch ( Exception $e )
        {
            // If the DB connection fails, output the error
            die ( $e->getMessage() );

        }

    }


    public static function getInstance()
    {
        if (is_null(self::$_singleton)) {
            self::$_singleton = new DB1();
        }
        return self::$_singleton;

    }


     //  Defines für alle PDO-Methoden
    public function __call ( $method, $args )
    {
        if ( is_callable(array($this->_connection, $method)) ) {
            return call_user_func_array(array($this->_connection, $method), $args);
        }
        else {
            throw new BadMethodCallException('Undefined method Database::' . $method);
        }
    }




}