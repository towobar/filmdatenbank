<?php

namespace models;


class Director {


    private   $id = '';
    private   $name = '';

    public function __construct(array $daten = array())
    {
        $this->setDaten($daten);
    }

    public function setDaten(array $daten)
    {
        // wenn $daten nicht leer ist, rufe die passenden Setter auf
        if ($daten) {

            foreach ($daten as $k => $v) {

                $setterName = 'set' . ucfirst($k);

                // pruefe ob ein passender Setter existiert
                if (method_exists($this, $setterName)) {

                    $this->$setterName($v); // Setteraufruf
                }
            }
        }
    }


    public function getId()
    {

        return  $this->id;

    }

    public function getName()
    {

        return  $this->name;

    }

    public function setId($id)
    {

        $this->id = $id;

    }

    public function setName($name)
    {

        $this->name = $name;

    }







}