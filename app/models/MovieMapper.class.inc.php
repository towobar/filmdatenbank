<?php
/**
 * MovieMapper
 */


namespace App\Models;

class MovieMapper
{


    private $dB = null;


    public function __construct($dB)
    {
        // Datenbank Object initialisieren

        $this->dB = $dB;

       //var_dump($this->dB);
    }


    public   function getMovieOverview($id)
    {

        $movieOverview = null;

        $sql = "SELECT  *  FROM filme   WHERE id = :id ";

        $data = array( 'id' => $id );

        $dbSelect = $this->dB->prepare($sql);

        $dbSelect->execute($data);

        try{

            $row = $dbSelect->fetch();

            $movieOverview = new Movie($row);

            $movieOverview->setGenre($this->getMovieGenres($id));

            $movieOverview->setSchauspieler($this->getActors($id));

        }
        catch(PDOException $e)
        {

            $movieOverview = null;
        }

        unset($dbSelect);

        return $movieOverview;


    }

    private function getMovieGenres($filmId)
    {

        $genres = array();
        $genresStr =  '';

        $sql = "SELECT genre.name FROM genre

        INNER JOIN film_genre ON genre.id = film_genre.genere_id

        WHERE film_genre.film_id = :filmId";

        $data = array( 'filmId' => $filmId );

        $dbSelect = $this->dB->prepare($sql);

        $dbSelect->execute($data);

        try {

            while($row = $dbSelect->fetch()){

                $genres[] = $row['name'];
            }

        }
        catch(PDOException $e)
        {

            echo 'catch';
            $genres = 'GENRE ...';
        }


        $genresStr = implode ( ' , ', $genres );

        $genresStr = strtoupper($genresStr);

        return $genresStr;

    }

    private function getActors($filmId)
    {

        $actors = array();
        $actorsStr =  '';

        $sql = "SELECT schauspieler.name FROM schauspieler

        INNER JOIN film_schauspieler ON schauspieler.id = film_schauspieler.schauspieler_id

        WHERE film_schauspieler.film_id = :filmId";

        $data = array( 'filmId' => $filmId );

        $dbSelect = $this->dB->prepare($sql);

        $dbSelect->execute($data);

        try {

            while($row = $dbSelect->fetch()){

                $actors[] = $row['name'];
            }

        }
        catch(PDOException $e)
        {

            echo 'catch';
            $genres = 'GENRE ...';
        }


        $actorsStr = implode ( ' , ', $actors );

        $actorsStr = strtoupper($actorsStr) . ' ...';

        return $actorsStr;

    }

    public    function getMoviesTitleAndId()
    {

        $data = array();


        //var_dump($this->dB);

        $objectArray = array();

        $sql = "SELECT titel,id  FROM filme ORDER BY titel ASC ";

        $dbSelect = $this->dB->prepare($sql);

        $dbSelect->execute();


        try{

            while($row = $dbSelect->fetch()){

                // Klasse initialisieren und Daten (assoziatives array) übergeben
                // Die setter werden im __construct von Movie über das array befüllt

                $movieList = new Movie($row);

                // Object an das array anhängen
                $objectArray[] = $movieList;

            }


        }
        catch(PDOException $e)
        {

            $objectArray = null;
        }

        unset($dbSelect);


        return $objectArray;


    }

    public function getMoviesCoverAndId()
    {

        $objectArray = array();

        $sql = "SELECT bild,id  FROM filme ";

        $dbSelect = $this->dB->prepare($sql);
        $dbSelect->execute();


        try{

            while($row = $dbSelect->fetch()){

                // Klasse initialisieren und Daten (assoziatives array) übergeben
                // Die setter werden im __construct von Movie über das array befüllt

                $movieList = new Movie($row);

                // Object an das array anhängen
                $objectArray[] = $movieList;

            }


        }
        catch(PDOException $e)
        {

            $objectArray = null;
        }

        unset($dbSelect);


        return $objectArray;


    }


    public function getMoviesByGenre($genre)
    {

        $objectArray = array();
        $filmIdArray = array();


        $genre = intval($genre);

        // 1. Schritt die filmIds der genere ID ermitteln

        $sql = "SELECT  film_id FROM film_genre WHERE  genere_id = :genreID  ";


        $data = array( 'genreID' => $genre );


        $dbSelect = $this->dB->prepare($sql);

        $dbSelect->execute($data);


        try{

            while($row = $dbSelect->fetch()){

                // film_id an das array anhängen
                $filmIdArray[] = $row['film_id'];

            }


            // 2. Schritt das FilmIDArray durchlaufen, ein movie Object erzeugen und in das objectArray anhängen.


            $sql = "SELECT *  FROM filme WHERE  id = :id ";

            $dbSelect = $this->dB->prepare($sql);

            $length = count($filmIdArray);

             for($i=0;$i<$length;$i++)
             {

                 $data = array( 'id' => $filmIdArray[$i] );


                 $dbSelect->execute($data);

                 $row = $dbSelect->fetch();

                 $movie = new Movie($row);

                 $objectArray[] = $movie;

             }


            // var_dump($objectArray); exit;


        }
        catch(PDOException $e)
        {

            $objectArray = null;
        }

        unset($dbSelect);


        return $objectArray;


    }


    public function getMoviesByActor($actorId)
    {

        $objectArray = array();
        $filmIdArray = array();


        $actorId = intval($actorId);

        // 1. Schritt die filmIds der genere ID ermitteln

        $sql = "SELECT  film_id FROM film_schauspieler WHERE  schauspieler_id = :actorID  ";


        $data = array( 'actorID' => $actorId );


        $dbSelect = $this->dB->prepare($sql);

        $dbSelect->execute($data);


        try{

            while($row = $dbSelect->fetch()){

                // film_id an das array anhängen
                $filmIdArray[] = $row['film_id'];

            }


            // 2. Schritt das FilmIDArray durchlaufen, ein movie Object erzeugen und in das objectArray anhängen.


            $sql = "SELECT *  FROM filme WHERE  id = :id ";

            $dbSelect = $this->dB->prepare($sql);

            $length = count($filmIdArray);

            for($i=0;$i<$length;$i++)
            {

                $data = array( 'id' => $filmIdArray[$i] );


                $dbSelect->execute($data);

                $row = $dbSelect->fetch();

                $movie = new Movie($row);

                $objectArray[] = $movie;

            }


            // var_dump($objectArray); exit;


        }
        catch(PDOException $e)
        {

            $objectArray = null;
        }

        unset($dbSelect);


        return $objectArray;


    }


    public function getMoviesByDirector($directorName)
    {

        $objectArray = array();

        // Zur Sicherheit !
        $directorName = trim($directorName);


        $sql = "SELECT *  FROM filme WHERE  regisseur = :director ";

        $dbSelect = $this->dB->prepare($sql);

        $data = array( 'director' => $directorName );

        $dbSelect->execute($data);


        while($row = $dbSelect->fetch() )
        {

            $movie = new Movie($row);

            $objectArray[] = $movie;


        }



        return $objectArray;
    }


    public function getMoviesByYear($yearString)
    {

        $objectArray = array();


        // Zur Sicherheit !
        $yearString = trim($yearString);

        $sql = '';

        switch($yearString)
        {

            case '1920-1950' :

                $sql = "SELECT *  FROM filme WHERE  (jahr >= 1920) AND (jahr <= 1950)";

                break;

            case '1951-1990' :

                $sql = "SELECT *  FROM filme WHERE  (jahr >= 1951) AND (jahr <= 1990)";

                break;
            case '1991-2000' :

                $sql = "SELECT *  FROM filme WHERE  (jahr >= 1991) AND (jahr <= 2000)";

                break;
            case '2001-Today' :

                $sql = "SELECT *  FROM filme WHERE  (jahr >= 2001) ";

                break;
        }



        $dbSelect = $this->dB->prepare($sql);



        $dbSelect->execute();


        while($row = $dbSelect->fetch() )
        {

            $movie = new Movie($row);

            $objectArray[] = $movie;


        }



        return $objectArray;
    }


    public function getMoviesByParams($searchString)
    {

        $objectArray = array();

        $searchString = trim($searchString);

        $searchString = '%' . $searchString .  '%';

        $sql = "SELECT *  FROM filme WHERE  titel LIKE :search";

        $dbSelect = $this->dB->prepare($sql);


        $data = array ( 'search'=> $searchString );

        $dbSelect->execute($data);


        while($row = $dbSelect->fetch() )
        {

            $movie = new Movie($row);

            $objectArray[] = $movie;


        }



        return $objectArray;



    }






    public function  insertMovie(Movie $movie)
    {

       $success = 'Error : Insert !';

        $sqlString = "INSERT  INTO filme (titel,jahr,dauer,regisseur,beschreibung,rating,imdbid,bild)
                      VALUES (:titel,:jahr,:dauer,:regisseur,:beschreibung,:rating,:imdbid,:bild)";

        try
        {
           // 1. Schritt : insert in filme

            $dbInsert  =  $this->dB->prepare($sqlString);

            $data = array(  ':titel' => $movie->getTitel(),
                            ':jahr' => $movie->getJahr(),
                            ':dauer' => $movie->getDauer(),
                            ':regisseur' => $movie->getRegisseur(),
                            ':beschreibung' => $movie->getBeschreibung(),
                            ':rating' => $movie->getRating(),
                            ':imdbid' => $movie->getImdbid(),
                            ':bild'=>$movie->getBild() );

            $dbInsert->execute($data);

            // Die id des neuen FilmEintrages
            $movie->setId($this->dB->lastInsertId()) ;


            // 2. Schritt Insert Genres in film_genre

             // Der String mit den Genres mit ',' getrennt

             $this->insertGenres($movie);


            // 3. Schritt Insert Schauspiler in film_schauspieler

            $this->insertActors($movie);



            $success = 'Insert Successful !';
        }
        catch(PDOException $e)
        {

            $success = 'Error : Insert !';
        }


        return $success;


    }

    /*
     * Diese Function liest den Genres String des neuen Films aus
     * und trägt die Genres mit der FilmID in die zwischen
     * Tabelle film_Genre ein
     *
     *
     */
    private function insertGenres(Movie $movie)
    {

        $genres = array();

         // genresString in ein Array umwandeln
        $genres = explode(',', $movie->getGenre());

        $length = count($genres);


        $sql = "  INSERT INTO film_genre (film_id,genere_id) VALUES (:filmId, (SELECT id FROM genre WHERE name=:name))";

        $dbInsert  =  $this->dB->prepare($sql);

        for($i=0;$i< $length;$i++)
        {
            // Achtung wichtig da sonst die Genrenamen in der Tabelle nicht gefunden werden
            $name = trim($genres[$i]);

            $data = array(  ':filmId' => $movie->getId(),
                            ':name' => $name );

            $dbInsert->execute($data);

        }


        //unset( $dbInsert);


    }

     /*
     * Diese Function liest den Actors String des neuen Films aus
     * und trägt die Actors mit der FilmID in die zwischen
     * Tabelle film_schauspieler ein
     *
     */
    private function insertActors(Movie $movie)
    {

        $actors = array();

        // genresString in ein Array umwandeln
        $actors = explode(',', $movie->getSchauspieler());

        $length = count($actors);


        $sql = "INSERT INTO film_schauspieler (film_id,schauspieler_id) VALUES (:filmId, (SELECT id FROM schauspieler WHERE name=:name))";

        $dbInsert  =  $this->dB->prepare($sql);

        for($i=0;$i< $length;$i++)
        {
            // Achtung wichtig da sonst die actornamen in der Tabelle nicht gefunden werden
            $name = trim($actors[$i]);

            $data = array(  ':filmId' => $movie->getId(),
                ':name' => $name );

            $dbInsert->execute($data);

        }


       // unset( $dbInsert);


    }



    public function updateMovie(Movie $movie)
   {

       $success = 'Error : Update !';


      // var_dump($movie->getId());


       $sqlString = "UPDATE  filme SET titel = :titel,jahr = :jahr,dauer = :dauer,
                          regisseur = :regisseur,beschreibung = :beschreibung,
                          rating = :rating,imdbid = :imdbid,bild = :bild  WHERE id = :id";
       try
       {
           // 1. Schritt : update in filme

           $dbInsert  =  $this->dB->prepare($sqlString);

           $data = array(

               ':id' => $movie->getId(),
               ':titel' => $movie->getTitel(),
               ':jahr' => $movie->getJahr(),
               ':dauer' => $movie->getDauer(),
               ':regisseur' => $movie->getRegisseur(),
               ':beschreibung' => $movie->getBeschreibung(),
               ':rating' => $movie->getRating(),
               ':imdbid' => $movie->getImdbid(),
               ':bild'=>$movie->getBild() );

           $dbInsert->execute($data);


           // 2. Schritt Delete Genres in film_genre, insert Genres in film_genre
           //    zum update werden die Einträge zuerst gelöscht !

            $this->deleteFilmGenres($movie);

            $this->insertGenres($movie);


           // 3. Schritt Schauspieler Schauspiler in film_schauspieler, insert Schauspieler in film_schauspieler
           //    zum update werden die Einträge zuerst gelöscht !


           $this->deleteFilmActors($movie);

           $this->insertActors($movie);



           $success = 'Update Successful !';
       }
       catch(PDOException $e)
       {

           $success = 'Error : Update !';
       }


       return $success;


   }

    private function deleteFilmGenres(Movie $movie)
    {

        $success = false;

        try
        {

            $sqlString = "DELETE FROM film_genre WHERE film_id = :id";

            $dbDelete =  $this->dB->prepare($sqlString);

            $data = array( 'id' => $movie->getId() );

            $dbDelete->execute($data);

            $success = $dbDelete->rowCount();


        }
        catch(PDOException $e)
        {
            $success = false;
        }

        return $success;

    }

    private function deleteFilmActors(Movie $movie)
    {

        $success = false;

        try
        {

            $sqlString = "DELETE FROM film_schauspieler WHERE film_id = :id";

            $dbDelete =  $this->dB->prepare($sqlString);

            $data = array( 'id' => $movie->getId() );

            $dbDelete->execute($data);

            $success = $dbDelete->rowCount();


        }
        catch(PDOException $e)
        {
            $success = false;
        }

        return $success;

    }


    public function deleteMovie(Movie $movie)
    {


        $success = 'Error Delete';

        try
        {

            // 1. Schritt Delete movie in filme

            $sqlString = "DELETE FROM filme WHERE id = :id";

            $dbDelete =  $this->dB->prepare($sqlString);

            $data = array( 'id' => $movie->getId() );

            $dbDelete->execute($data);

           // $success = $dbDelete->rowCount();


            // 2. Schritt Delete Einträge in film_genre

            $this->deleteFilmGenres($movie);

            // 3. Schritt Delete Einträge in film_schauspieler

            $this->deleteFilmActors($movie);


            $success = 'Delete ' . $movie->getTitel()  .  ' successful !';

        }
        catch(PDOException $e)
        {
            $success = 'Error Delete';
        }

        return $success;






    }






}