<?php
/**
 * Movie Class
 */


namespace App\Models;

use JsonSerializable;


class Movie implements JsonSerializable {


    private   $id = '';
    private   $titel = '';
    private   $bild = '';
    private   $jahr = '';
    private   $dauer = '';
    private   $beschreibung = '';
    private   $regisseur = '';
    private   $schauspieler = '';
    private   $genre = '';
    private   $rating = '';
    private   $imdbid = '';

    public function __construct(array $daten = array())
    {
        $this->setDaten($daten);
    }

    public function setDaten(array $daten)
    {
        // wenn $daten nicht leer ist, rufe die passenden Setter auf
        if ($daten) {

            foreach ($daten as $k => $v) {

                $setterName = 'set' . ucfirst($k);

                // pruefe ob ein passender Setter existiert
                if (method_exists($this, $setterName)) {

                    $this->$setterName($v); // Setteraufruf
                }
            }
        }
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }


    public function getId()
    {

        return  $this->id;

    }

    public function getTitel()
    {

        return  $this->titel;

    }

    public function getBild()
    {

        return  $this->bild;

    }

    public function getJahr()
    {

        return  $this->jahr;

    }


    public function getDauer()
    {

        return  $this->dauer;

    }


    public function getBeschreibung()
    {

        return  $this->beschreibung;

    }

    public function getRegisseur()
    {

        return  $this->regisseur;

    }

    public function getGenre()
    {

        return  $this->genre;

    }

    public function getRating()
    {

        return  $this->rating;

    }


    public function getImdbid()
    {

        return  $this->imdbid;

    }


    public function getSchauspieler()
    {

        return  $this->schauspieler;

    }


    public function setId($id)
    {

        $this->id = $id;

    }

    public function setTitel($titel)
    {

        $this->titel = $titel;

    }

    public function setBild($bild)
    {

        $this->bild = $bild;

    }

    public function setJahr($jahr)
    {

        $this->jahr = $jahr;

    }

    public function setDauer($dauer)
    {

        $this->dauer = $dauer;

    }

    public function setbeschreibung($beschreibung)
    {

        $this->beschreibung = $beschreibung;

    }

    public function setRegisseur($regisseur)
    {

        $this->regisseur = $regisseur;

    }

    public function setGenre($genre)
    {

        $this->genre = $genre;

    }

    public function setRating($rating)
    {

        $this->rating = $rating;

    }

    public function setImdbid($imdbid)
    {

        $this->imdbid = $imdbid;

    }

    public function setSchauspieler($schauspieler)
    {

        $this->schauspieler = $schauspieler;

    }


}