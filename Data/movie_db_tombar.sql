

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `movie_db_tombar`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `benutzer`
--

CREATE TABLE IF NOT EXISTS `benutzer` (
`id` int(4) NOT NULL,
  `name` varchar(40) NOT NULL,
  `passwort` varchar(40) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Daten für Tabelle `benutzer`
--

INSERT INTO `benutzer` (`id`, `name`, `passwort`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `filme`
--

CREATE TABLE IF NOT EXISTS `filme` (
`id` int(4) NOT NULL,
  `titel` varchar(80) NOT NULL,
  `jahr` int(11) NOT NULL,
  `dauer` int(11) NOT NULL,
  `beschreibung` text NOT NULL,
  `regisseur` varchar(80) NOT NULL,
  `rating` decimal(2,1) DEFAULT NULL,
  `imdbid` char(10) DEFAULT NULL,
  `bild` varchar(80) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=54 ;

--
-- Daten für Tabelle `filme`
--

INSERT INTO `filme` (`id`, `titel`, `jahr`, `dauer`, `beschreibung`, `regisseur`, `rating`, `imdbid`, `bild`) VALUES
(1, 'The Godfather', 1972, 175, 'The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.', 'Francis Ford Coppola', '9.2', 'tt0068646', 'godfather'),
(2, 'Pulp Fiction', 1994, 154, 'The lives of two mob hit men, a boxer, a gangster''s wife, and a pair of diner bandits intertwine in four tales of violence and redemption.', 'Quentin Tarantino', '8.9', 'tt0110912', 'pulpFiction'),
(3, 'The Lord of the Rings: The Fellowship of the Ring', 2001, 178, 'A meek hobbit of the Shire and eight companions set out on a journey to Mount Doom to destroy the One Ring and the dark lord Sauron.', 'Peter Jackson', '8.8', 'tt0120737', 'lordOfRings'),
(4, 'Star Wars: Episode V - The Empire Strikes Back', 1980, 124, 'After the rebels have been brutally overpowered by the Empire on their newly established base, Luke Skywalker takes advanced Jedi training with Master Yoda, while his friends are pursued by Darth Vader as part of his plan to capture Luke.', 'Irvin Kershner', '8.8', 'tt0080684', 'starwars'),
(5, 'Forrest Gump', 1994, 142, 'Forrest Gump, while not intelligent, has accidentally been present at many historic moments, but his true love, Jenny Curran, eludes him.', 'Robert Zemeckis', '8.8', 'tt0109830', 'forrestgump'),
(6, 'One Flew Over the Cuckoo''s Nest', 1975, 133, 'Upon admittance to a mental institution, a brash rebel rallies the patients to take on the oppressive head nurse.', 'Milos Forman', '8.7', 'tt0073486', 'cuckoosNest'),
(7, 'The Matrix', 1999, 136, 'A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.', 'Andy Wachowski, Lana Wachowski', '8.7', 'tt0133093', 'matrix'),
(8, 'Star Wars: Episode IV - A New Hope', 1977, 121, 'Luke Skywalker joins forces with a Jedi Knight, a cocky pilot, a wookiee and two droids to save the universe from the Empire''s world-destroying battle-station, while also attempting to rescue Princess Leia from the evil Darth Vader.', 'George Lucas', '8.7', 'tt0076759', 'starWars77'),
(9, 'The Usual Suspects', 1995, 106, 'A sole survivor tells of the twisty events leading up to a horrific gun battle on a boat, which begin when five criminals meet at a seemingly random police lineup.', 'Bryan Singer', '8.7', 'tt0114814', 'usualSuspect'),
(10, 'Once Upon a Time in the West', 1968, 175, 'Epic story of a mysterious stranger with a harmonica who joins forces with a notorious desperado to protect a beautiful widow from a ruthless assassin working for the railroad.', 'Sergio Leone', '8.6', 'tt0064116', 'songOfDead'),
(11, 'Saving Private Ryan', 1998, 169, 'Following the Normandy Landings, a group of U.S. soldiers go behind enemy lines to retrieve a paratrooper whose brothers have been killed in action.', 'Steven Spielberg', '8.6', 'tt0120815', 'privateRyan'),
(12, 'Rear Window', 1954, 112, 'A wheelchair bound photographer spies on his neighbours from his apartment window and becomes convinced one of them has committed murder.', 'Alfred Hitchcock', '8.6', 'tt0047396', 'rearWindow'),
(13, 'The Intouchables', 2011, 112, 'After he becomes a quadriplegic from a paragliding accident, an aristocrat hires a young man from the projects to be his caregiver.', 'Olivier Nakache, Eric Toledano', '8.6', 'tt1675434', 'intouchables'),
(14, 'Terminator 2: Judgment Day', 1991, 137, 'A cyborg, identical to the one who failed to kill Sarah Connor, must now protect her nine-year-old son, John, from a more advanced cyborg, made out of liquid metal.', 'James Cameron', '8.5', 'tt0103064', 'terminator2'),
(15, 'Apocalypse Now', 1979, 153, 'During the Vietnam War, Captain Willard is sent on a dangerous mission into Cambodia to assassinate a renegade colonel who has set himself up as a god among a local tribe.', 'Francis Ford Coppola', '8.5', 'tt0078788', 'appocalypseNow'),
(16, 'Amelie', 2001, 122, 'Amelie is an innocent and naive girl in Paris with her own sense of justice. She decides to help those around her and, along the way, discovers love.', 'Jean-Pierre Jeunet', '8.4', 'tt0211915', 'amelie'),
(17, 'Das Boot', 1981, 150, 'The claustrophobic world of a WWII German U-boat; boredom, filth, and sheer terr', 'Wolfgang Peterson', '8.4', 'tt0082096', 'dasBoot'),
(18, 'Toy Story 3', 2010, 103, 'The toys are mistakenly delivered to a day-care center instead of the attic right before Andy leaves for college, and it''s up to Woody to convince the other toys that they weren''t abandoned and to return home.', 'Lee Unkrich', '8.4', 'tt0435761', 'toy3'),
(19, 'Braveheart ', 1995, 178, 'When his secret bride is executed for assaulting an English soldier who tried to', 'Mel Gibson', '8.4', 'tt0112573', 'braveHeart'),
(20, 'Lawrence of Arabia', 1962, 217, 'A flamboyant and controversial British military figure and his conflicted loyalt', 'David Lean', '8.4', 'tt0056172', 'lawrenceOfArabia'),
(21, 'The Sting', 1973, 129, 'In 1930s Chicago, a young con man seeking revenge for his murdered partner teams up with a master of the big con to win a fortune from a criminal banker.', 'George Roy Hill', '8.4', 'tt0070735', 'clou'),
(22, 'Inglourious Basterds', 2009, 153, 'In Nazi-occupied France during World War II, a plan to assassinate Nazi leaders by a group of Jewish U.S. soldiers coincides with a theatre owner''s vengeful plans for the same.', 'Quentin Tarantino', '8.3', 'tt0361748', 'inglouriusBastards'),
(53, 'Braveheart  2', 2015, 178, 'When his secret bride is executed for assaulting an English soldier who tried to', 'Mel Gibson', '8.4', 'tt0112573', 'braveHeart');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `film_genre`
--

CREATE TABLE IF NOT EXISTS `film_genre` (
  `film_id` int(4) DEFAULT NULL,
  `genere_id` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `film_genre`
--

INSERT INTO `film_genre` (`film_id`, `genere_id`) VALUES
(1, 14),
(1, 15),
(2, 14),
(2, 15),
(2, 21),
(3, 12),
(3, 16),
(4, 1),
(4, 12),
(4, 16),
(5, 9),
(5, 15),
(6, 15),
(7, 1),
(7, 20),
(8, 1),
(8, 12),
(8, 16),
(9, 14),
(9, 15),
(9, 21),
(10, 22),
(11, 1),
(11, 11),
(11, 15),
(12, 19),
(12, 21),
(13, 3),
(13, 13),
(13, 15),
(14, 1),
(14, 20),
(15, 11),
(15, 15),
(16, 3),
(16, 9),
(17, 11),
(17, 12),
(17, 15),
(18, 2),
(18, 3),
(18, 12),
(20, 12),
(20, 13),
(20, 15),
(21, 3),
(21, 14),
(21, 15),
(22, 11),
(22, 12),
(22, 15),
(19, 1),
(19, 13),
(19, 15),
(53, 1),
(53, 13),
(53, 15),
(54, NULL),
(55, NULL),
(56, NULL),
(57, NULL),
(58, NULL),
(59, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `film_schauspieler`
--

CREATE TABLE IF NOT EXISTS `film_schauspieler` (
  `film_id` int(4) DEFAULT NULL,
  `schauspieler_id` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `film_schauspieler`
--

INSERT INTO `film_schauspieler` (`film_id`, `schauspieler_id`) VALUES
(1, 2),
(1, 3),
(2, 4),
(2, 5),
(3, 1),
(4, 6),
(4, 12),
(5, 8),
(5, 9),
(6, 10),
(7, 11),
(8, 6),
(8, 12),
(9, 13),
(10, 14),
(10, 15),
(11, 16),
(11, 8),
(12, 17),
(12, 18),
(13, 19),
(13, 20),
(14, 21),
(14, 22),
(15, 23),
(15, 2),
(16, 24),
(16, 25),
(17, 26),
(17, 27),
(18, 28),
(18, 8),
(20, 31),
(20, 32),
(21, 33),
(21, 34),
(22, 35),
(22, 36),
(19, 6),
(53, 21),
(54, NULL),
(55, NULL),
(56, NULL),
(57, NULL),
(58, NULL),
(59, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `genre`
--

CREATE TABLE IF NOT EXISTS `genre` (
`id` int(4) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Daten für Tabelle `genre`
--

INSERT INTO `genre` (`id`, `name`) VALUES
(1, 'Action'),
(2, 'Animation'),
(3, 'Comedy'),
(4, 'Documentary'),
(5, 'Family'),
(6, 'Film-Noir'),
(7, 'Horror'),
(8, 'Musical'),
(9, 'Romance'),
(10, 'Sport'),
(11, 'War'),
(12, 'Adventure'),
(13, 'Biography'),
(14, 'Crime'),
(15, 'Drama'),
(16, 'Fantasy'),
(17, 'History'),
(18, 'Music'),
(19, 'Mystery'),
(20, 'Sci-Fi'),
(21, 'Thriller'),
(22, 'Western');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `regisseur`
--

CREATE TABLE IF NOT EXISTS `regisseur` (
`id` int(4) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Daten für Tabelle `regisseur`
--

INSERT INTO `regisseur` (`id`, `name`) VALUES
(1, 'Francis Ford Coppola'),
(2, 'Quentin Tarantino'),
(3, 'Peter Jackson'),
(4, 'Irvin Kershner'),
(5, 'Robert Zemeckis'),
(6, 'Milos Forman'),
(7, 'Andy Wachowski'),
(8, 'George Lucas'),
(9, 'Bryan Singer'),
(10, 'Sergio Leone'),
(11, 'Steven Spielberg'),
(12, 'Alfred Hitchcock'),
(13, 'Olivier Nakache'),
(14, 'James Cameron'),
(16, 'Jean-Pierre Jeunet'),
(17, 'Wolfgang Petersen'),
(18, 'Lee Unkrich'),
(19, 'Mel Gibson'),
(20, 'David Lean'),
(21, 'George Roy Hill'),
(23, 'Eli Roth');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `schauspieler`
--

CREATE TABLE IF NOT EXISTS `schauspieler` (
`id` int(4) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

--
-- Daten für Tabelle `schauspieler`
--

INSERT INTO `schauspieler` (`id`, `name`) VALUES
(1, 'Alan Howard'),
(2, 'Marlon Brando '),
(3, 'Al Pacino'),
(4, 'Laura Lovelace'),
(5, 'John Travolta'),
(6, 'Harrison Ford'),
(7, 'Carrie Fisher'),
(8, 'Tom Hanks'),
(9, 'Sally Field'),
(10, 'Michael Berryman'),
(11, 'Keanu Reeves'),
(12, 'Mark Hamill'),
(13, 'Stephen Baldwin'),
(14, 'Claudia Cardinale'),
(15, 'Henry Fonda'),
(16, 'Tom Sizemore'),
(17, 'James Stewart'),
(18, 'Grace Kelly'),
(19, 'François Cluzet'),
(20, 'Omar Sy'),
(21, 'Arnold Schwarzenegger'),
(22, 'Linda Hamilton'),
(23, 'Martin Sheen'),
(24, 'Audrey Tautou'),
(25, 'Mathieu Kassovitz'),
(26, 'Jürgen Prochnow'),
(27, 'Herbert Grönemeyer'),
(28, 'Tim Allen'),
(29, 'James Robinson'),
(30, 'Sean Lawlor'),
(31, 'Peter O''Toole'),
(32, 'Anthony Quinn'),
(33, 'Paul Newman'),
(34, 'Robert Redford'),
(35, 'Brad Pitt'),
(36, 'Christoph Waltz');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `benutzer`
--
ALTER TABLE `benutzer`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `filme`
--
ALTER TABLE `filme`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genre`
--
ALTER TABLE `genre`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regisseur`
--
ALTER TABLE `regisseur`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schauspieler`
--
ALTER TABLE `schauspieler`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `benutzer`
--
ALTER TABLE `benutzer`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `filme`
--
ALTER TABLE `filme`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `genre`
--
ALTER TABLE `genre`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `regisseur`
--
ALTER TABLE `regisseur`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `schauspieler`
--
ALTER TABLE `schauspieler`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
